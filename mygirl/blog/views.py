from django.shortcuts import render,get_object_or_404
from django.utils import timezone
from django.shortcuts import redirect
from .forms import PostForm
from .models import Post

# Create your views here.
def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    return render(request, 'blog/post_list.html', {'posts':posts})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})

    
def post_new(request):
    #Todos los campos del formulario estan ahora en request.POST
    if request.method == "POST":  #al vista con los datos del formulario que acabamos de ingresar, formulario rellenado
        form =PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit = False) # guardar formulario, que no queremos guardar el modelo post aún, queremos añdir la autora primero
            post.author = request.user # campo obligatorio, añadir autora
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail',pk=post.pk) # ve a la vista 
    else:#accedemos al formulario por 1r vez, formulario vacio
        form =PostForm() 
    return render(request,'blog/post_edit.html',{'form':form})


def post_edit(request,pk):
    post= get_object_or_404(Post,pk=pk)
    if request.method=="POST":
        form=PostForm(request.POST, instance=post) # pasamos este post como una instancia tanto al guardar el formulario
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail',pk=post.pk)
    else:
        form =PostForm(instance=post)
    return render (request,'blog/post_edit.html',{'form':form})