from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator,EmailValidator,MinLengthValidator


class User(models.Model):
    first_name = models.CharField(
    max_length=30,
    validators=[
        RegexValidator(
            regex='^[a-zA-Z]+$',
            message='Firt_name must be letters',
            code='invalid_firt_name'
        ),
    ]
    )
    last_name = models.CharField(
    max_length=30,
    validators=[
        RegexValidator(
            regex='^[a-zA-Z]+$',
            message='Last_name must be letters',
            code='invalid_last_name'
        ),
    ]
    )
    birth_date = models.DateField(default=timezone.now, blank=True,null=True)
    email = models.EmailField(max_length=254, validators=[
        EmailValidator(
            message='Enter a valid email address',
            code='invalid_email'
        ),
    ])
    telephone = models.PositiveIntegerField( blank=True,null=True)
    address = models.CharField(max_length=80)
    postal_code = models.PositiveIntegerField( blank=True,null=True)
    city =models.CharField(max_length=100, default="Barcelona")
    state = models.CharField(max_length=100, blank=True,null=True)
    country = models.CharField(max_length=100, blank=True,null=True)
    username = models.CharField(max_length=30,validators=[
        MinLengthValidator(
            limit_value= 10,
            message='Enter more characters, minimum length 10 characters'
        ),
    ])
    password = models.CharField(max_length=50)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)


