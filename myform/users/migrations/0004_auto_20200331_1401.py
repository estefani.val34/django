# Generated by Django 3.0.3 on 2020-03-31 14:01

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20200331_1400'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='username',
            field=models.CharField(max_length=30, validators=[django.core.validators.MinLengthValidator(limit_value=10, message='Enter more characters, minimum length 10 characters')]),
        ),
    ]
