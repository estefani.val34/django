from django.db import models

# Create your models here.


class Subscription(models.Model):
    email = models.CharField(max_length=100)

class Contact(models.Model):
    title = models.CharField(max_length=150)
    message = models.CharField(max_length=200)