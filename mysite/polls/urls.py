from django.urls import path

from . import views



urlpatterns = [
    path('', views.form_redir, name='form_redir'),
    path('muf', views.multiple_forms, name='multiple_forms'),
    path('mufd', views.MultipleFormsDemoView.as_view(), name='MultipleFormsDemoView')
]
