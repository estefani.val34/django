from django import forms

from .models import User


SUBMISSION_TYPE_CHOICES = [
    ('sequence', 'Sequence'),
    ('array', 'Array'),
    ('phenotype', 'Phenotype'),
]

class UserForm(forms.ModelForm):

    class Meta:
            model = User
            fields = ('__all__')
            exclude = ['created_date']

            widgets={
            'submission_type': forms.Select(choices=SUBMISSION_TYPE_CHOICES,attrs={'class':'form-control'}),
            }
   
class LoginForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('box_number', 'password')
