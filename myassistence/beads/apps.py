from django.apps import AppConfig


class BeadsConfig(AppConfig):
    name = 'beads'
