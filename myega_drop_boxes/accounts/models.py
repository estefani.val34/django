from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator

# las cuentas asignadas . Un cuneta asignada tene diferentes emails, users
class SubmissionBoxes(models.Model):
    # pk, number, unico, se creará si sale como disponible en la base de ega_box disponibles
    ega_box_number = models.PositiveIntegerField(primary_key=True, unique=True)
    account_type = models.CharField(
        max_length=30, blank=True, null=True)  # texto no muy largo
    temporary_column = models.CharField(
        max_length=30, blank=True, null=True)  # texto
    account_opened_date = models.DateTimeField(
        default=timezone.now)  # fecha de hoy defecto
    password_changed_date = models.DateTimeField(blank=True, null=True)  # date
    rt_ticket = models.CharField(max_length=30, unique=True)  # ?
    emails = models.CharField(max_length=300)
    comments = models.TextField(blank=True, null=True)  # texto largo
    # default False, no hemos recibido el dta del cliente
    received_dta = models.BooleanField(default=False)
    # default False, no hemos enviado el dta al cliente
    sent_dta = models.BooleanField(default=False)
    days_passed_since_created_date = models.PositiveIntegerField(
        blank=True, null=True)  # defecto dias de la fecha account_opened_date- now

    class Meta:
        db_table = 'submissionboxes'
        

    def __str__(self):
        return str(self.ega_box_number)

    def get_users(self):
        return ', '.join(self.users.all().values_list('email', flat=True))





class User(models.Model):
    email = models.EmailField(max_length=100 ) # emails multiples, ajuntar por coma + espacio
    submissionboxes = models.ForeignKey(
        SubmissionBoxes,
        related_name='users', on_delete=models.CASCADE)

    class Meta:
        db_table = 'user'

    def __str__(self):
        return self.email



# las cuentas creadas2
class SubmissionAccounts(models.Model):
    # emails text email/s que se concatenaran con comas +espacios
    ega_box_number = models.PositiveIntegerField(unique=True,  primary_key=True)   # ega_ega_box_number  primary key number
    account_accession = models.CharField(max_length=50, unique=True)# text automatico, se recupera de AccountStableIDs
    emails = models.CharField(max_length=300)
    submission_type = models.CharField(max_length=60, blank=True, null=True) # text 3 opciones: (Sequence, Array, Phenotype)
    country = models.CharField(max_length=100)  # text autocompletado
    region = models.CharField(max_length=100)  # text
    institute_name = models.CharField(max_length=100)  # text autocompletado
    mapi = models.CharField(max_length=10, blank=True, null=True)  # text
    affiliation_project = models.CharField( max_length=50, blank=True, null=True)  # text
    center_name = models.CharField(max_length=80)  # text  autocompletado
    password = models.CharField(max_length=80)  # text
    comments = models.TextField(blank=True, null=True)  # text
    submission_box = models.ForeignKey(SubmissionBoxes, on_delete=models.CASCADE)  # One to One filed, hacer que el valor lo guarde por defecto
    created_date = models.DateTimeField(default=timezone.now)  # date now default

    def __str__(self):
        return '%s %s %s' % (self.ega_box_number, self.account_accession, self.emails)

    def get_counthry(self):
        self.country=', '.join(self.counthry.all().values_list('name', flat=True))
        return  self.country


class Country(models.Model):
    name = models.CharField(max_length=600)
    submissionAccounts_country = models.ForeignKey(
        SubmissionAccounts,
        related_name='counthry', on_delete=models.CASCADE)

    class Meta:
        db_table = 'country'

    def __str__(self):
        return self.name




# Relaciona los ega-box numbers y account accession
# de las cuentas creadas
class AccountStableIDs(models.Model):
    ega_box_text = models.CharField(primary_key=True, max_length=100, unique=True)
    submission_boxes = models.OneToOneField(
        SubmissionBoxes,
        on_delete=models.CASCADE
    )
    # text, pk, unique, fk de las cuentas creadas

    submission_account = models.OneToOneField(
        SubmissionAccounts,
        on_delete=models.CASCADE,
        blank=True, null=True
    )
    # text, pk, unique, fk de las cuentas creadas
    account_accession = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return '%s %s' % (self.ega_box_text, self.account_accession)

# cuentas reservadas para descargar archivos,


class DownloadBoxes(models.Model):
    # ega_box_number , puedo estar o no en Submission account, one to one filed ?, pk
    box = models.PositiveIntegerField(primary_key=True)
    account_type = models.CharField(max_length=20)  # text
    re_distribution = models.BooleanField(default=False)  # text,
    tempory_col = models.CharField(max_length=60)  # text
    # email/s hay problema si tienen el mismo nombre,
    user = models.EmailField(max_length=70)
    account_opened_date = models.DateTimeField()
    # ? date preguntar para que sirve Aina . Relacionada con las cuentas creadas
    password_changed_date = models.DateTimeField()
    expired = models.BooleanField(default=False)  # boolen
    comments = models.TextField()  # text

    def __str__(self):
        return '%s %s' % (self.box, self.user)


# El Box_history te da el historial de uso
# de  los ega-box numbers (que usuarios han
# tenido ese ega-box number )
# cuando se crea la cuenta, tb se guarda en el historial
class BoxHistory(models.Model):
    # text ega_box_ number, no pk pr se puede repetir
    ega_name = models.CharField(max_length=100)
    user = models.EmailField(max_length=70)  # email/s
    account_opened_date = models.DateTimeField()  # date
    password_changed_date = models.DateTimeField()  # date
    comments = models.TextField()  # text
    created_date = models.DateTimeField(default=timezone.now)
# tiene el ega box numbers disponibles , el limite y el numero que hay

    def __str__(self):
        return '%s %s' % (self.ega_name, self.user)


# tabla de ega box numbers que estan disponibles 

class EgaBoxNumbersAvailable(models.Model):
    ega_numbers_disponibles = models.CharField(max_length=1000,primary_key=True) # string de los ega_box numbers disponibles
    ega_maxim =  models.PositiveIntegerField()
    quantity =  models.IntegerField() 
