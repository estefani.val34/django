from django.shortcuts import render, redirect, reverse, get_list_or_404, get_object_or_404
from django.http import HttpResponse
from django.template import loader
from django.views import generic
from django.utils import timezone
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from .forms import (
    SubmissionBoxesModelForm,
    UserFormset,
    FindSubmissionAccountForm,
    ModifySubBoxesModelForm,
    SubmissionAccountsModelForm,
    CountryFormset,
    EgaBoxNumbersAvailableModelForm
)
from .models import (
    SubmissionBoxes, 
    User, 
    SubmissionAccounts,
    Country,
    EgaBoxNumbersAvailable, 
    AccountStableIDs)
from django.core import serializers
import json


class SubmissionAccountDetailView(DetailView):
    model = SubmissionAccounts
    template_name = 'accounts/account_detail.html'


class SubmissionBoxesDetailView(DetailView):
    model = SubmissionBoxes
    template_name = 'accounts/submission_boxes_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context


class AccountStableIDsListView(generic.ListView):
    model = AccountStableIDs
    context_object_name = 'accountStableIDs'
    template_name = 'accounts/accountStableIDs_list.html'


class SubmissionboxesListView(generic.ListView):
    model = SubmissionBoxes
    context_object_name = 'submissionboxes'
    template_name = 'accounts/list.html'

    def get_queryset(self):
        """Return all objects ordered by ega_box_number."""
        return SubmissionBoxes.objects.order_by('-account_opened_date')


class SubmissionAccountListView(generic.ListView):
    model = SubmissionAccounts
    context_object_name = 'accounts'
    template_name = 'accounts/account_list.html'

    def get_queryset(self):
        """Return all objects ordered by ega_box_number."""
        return SubmissionAccounts.objects.order_by('-created_date')


class EgaBoxNumbersAvailableListView(generic.ListView):
    model = EgaBoxNumbersAvailable
    context_object_name = 'numbers'
    template_name = 'accounts/ega_numbers_available_list.html'


def create_submissionboxes_with_users(request):
    template_name = 'accounts/submission_boxes_new.html'
    # GET es obtener infromacion del srvidor . Traer info. que esta en el servidor (base de datos)
    #
    ega_numbers_uso = list(SubmissionBoxes.objects.all(
    ).values_list('ega_box_number', flat=True))
    ega_numbers_disponibles = []

    for i in range(1, 35):
        if i not in ega_numbers_uso:
            ega_numbers_disponibles.append(i)
    pstr = ";".join([str(num) for num in ega_numbers_disponibles])
    list_int = [int(num) for num in pstr.split(";")]
    quan = len(list_int)
    #
    if request.method == 'GET':
        submissionboxesform = SubmissionBoxesModelForm(request.GET or None)
        formset = UserFormset(queryset=User.objects.none())
    # POST enviar info. desde el cliente para que sea procesada y actualice o agrugue inf. en el servidor(base de datos)
    elif request.method == 'POST':
        submissionboxesform = SubmissionBoxesModelForm(request.POST)

        num_formset = int(''.join(request.POST['form-TOTAL_FORMS']))
        list_emails = []
        for i in range(0, num_formset):
            list_emails.append(request.POST['form-'+str(i)+'-email'])
        string_emails = ', '.join(list_emails)
        formset = UserFormset(request.POST)
        if submissionboxesform.is_valid() and formset.is_valid():
            submissionboxes = submissionboxesform.save(commit=False)
            submissionboxes.emails = string_emails
            submissionboxes.save()
            ega_box_number = submissionboxesform.cleaned_data['ega_box_number']
            #
            ega_numbers_uso = list(SubmissionBoxes.objects.all(
            ).values_list('ega_box_number', flat=True))
            ega_numbers_disponibles = []

            for i in range(1, 35):
                if i not in ega_numbers_uso:
                    ega_numbers_disponibles.append(i)
            pstr = ";".join([str(num) for num in ega_numbers_disponibles])
            list_int = [int(num) for num in pstr.split(";")]
            quan = len(list_int)
            ega_a = EgaBoxNumbersAvailable.objects.create(
                ega_numbers_disponibles=pstr, ega_maxim=35, quantity=quan)
            #
            for form in formset:
                user = form.save(commit=False)
                user.submissionboxes = submissionboxes
                user.save()
            return redirect('accounts:submissionboxes_detail', ega_box_number)

    return render(request, template_name, {
        'submissionboxesform': submissionboxesform,
        'formset': formset,
        'pstr': pstr,
    })

# lo va a buscar en las dos tablas : submissiion boxes, submission account


def find_account(request):
    form = FindSubmissionAccountForm()
    mes_exists = ""
    if request.method == "POST":
        form = FindSubmissionAccountForm(request.POST)
        if form.is_valid():
            print("moamaoamao")
            ega_box_number = request.POST['ega_box']
            # exists submission boxes
            exists_sb = SubmissionBoxes.objects.filter(
                ega_box_number=ega_box_number).count()
            if exists_sb == 1:
                return redirect('accounts:sub_detail', pk=ega_box_number)
            else:
                mes_exists = "This ega box number does not exist. Not pre-assigned."
    return render(request, 'accounts/account_find.html', {'form': form, 'mes_exists': mes_exists})


def search(request):
    # ega_box_number = request.GET.get('ega_box_number')  # diccionario

    # subboxes = SubmissionBoxes.objects.filter(ega_box_number=ega_box_number)
    # subboxes = [subbox_serializer(subbox) for subbox in subboxes]  # lista de diccionarios

    # return HttpResponse(json.dumps(subboxes), content_type='application/json')
    pass


def subbox_serializer(subbox):
    return {'ega_box_number': subbox.ega_box_number, 'rt_ticket': subbox.rt_ticket,  'emails': subbox.emails, 'received_dta': subbox.received_dta}


def account_serializer(a):
    return {'ega_box_number': a.ega_box_number,
            'account_accession': a.account_accession,
            'emails': a.emails}


def searchega(request):
    ega_box_number = request.GET.get('ega_box')
    subboxes = SubmissionBoxes.objects.filter(ega_box_number=ega_box_number)
    account = SubmissionAccounts.objects.filter(ega_box_number=ega_box_number)
    if subboxes.exists():
        account = [account_serializer(a) for a in account]
        # lista de diccionarios
        subboxes = [subbox_serializer(subbox) for subbox in subboxes]
        subboxes = subboxes+account
    return HttpResponse(json.dumps(subboxes), content_type='application/json')

def create_book_with_authors(request):
    template_name = 'store/create_with_author.html'
    if request.method == 'GET':
        bookform = BookModelForm(request.GET or None)
        formset = AuthorFormset(queryset=Author.objects.none())
    elif request.method == 'POST':
        bookform = BookModelForm(request.POST)
        formset = AuthorFormset(request.POST)
        if bookform.is_valid() and formset.is_valid():
            # first save this book, as its reference will be used in `Author`
            book = bookform.save()
            for form in formset:
                # so that `book` instance can be attached.
                author = form.save(commit=False)
                author.book = book
                author.save()
            return redirect('store:book_list')
    return render(request, template_name, {
        'bookform': bookform,
        'formset': formset,
    })


def create_account(request, pk):
    post = get_object_or_404(SubmissionBoxes, pk=pk)
    template_name = 'accounts/account_create.html'
    #
    gebt = "ega-box-"+str(pk)
    get_accession = AccountStableIDs.objects.get(ega_box_text=gebt)
    account_accession = get_accession.account_accession
    #
    if request.method == 'GET':
        form = SubmissionAccountsModelForm(instance=post)
        formset = CountryFormset(queryset=Country.objects.none())
    elif request.method == "POST":
        form = SubmissionAccountsModelForm(request.POST)
        formset = CountryFormset(request.POST)
        if form.is_valid() and formset.is_valid():
            ega_box_number = request.POST['ega_box_number']

            post = form.save(commit=False)
            post.submission_box = SubmissionBoxes.objects.get(
                ega_box_number=ega_box_number)
            post.save()
            get_accession.submission_account = post
            get_accession.save()

            for form in formset:
                # so that `book` instance can be attached.
                country = form.save(commit=False)
                country.submissionAccounts_country = post
                country.save()

            return redirect('accounts:account_list')
   
    return render(request, template_name, {
        'form': form,
        'account_accession': account_accession,
        'formset': formset
        })


def modify_submissionboxes(request, pk):
    post = get_object_or_404(SubmissionBoxes, pk=pk)
    template_name = 'accounts/subBoxacount_detail.html'
    date = post.account_opened_date
    if request.method == "POST":
        received_dta = request.POST.get('received_dta', '')
        form = ModifySubBoxesModelForm(request.POST, instance=post)
        if form.is_valid():

            ega_box_number = request.POST['ega_box_number']
            exists_account = SubmissionAccounts.objects.all().filter(
                ega_box_number=ega_box_number).exists()
            if received_dta == "on":  # solo quiere añadir cambios true>on
                if exists_account == False:
                    #
                    sb = SubmissionBoxes.objects.get(
                        ega_box_number=ega_box_number)
                    ebt = "ega-box-"+str(sb.ega_box_number)
                    if AccountStableIDs.objects.all().count() == 0:  # es el primero
                        ac = "EGAB"+'1'.zfill(11)
                        create_accession = AccountStableIDs.objects.create(
                            ega_box_text=ebt, submission_boxes=sb, account_accession=ac)
                    else:  # mayor a 0
                        list_acs = list(AccountStableIDs.objects.all(
                        ).values_list('account_accession', flat=True))
                        maxi_ac = max(list_acs)
                        last = AccountStableIDs.objects.get(
                            account_accession=maxi_ac)
                        l_ac = last.account_accession
                        split_l_ac = l_ac.split('EGAB')
                        num_ac = int(split_l_ac[1])+1
                        ac = "EGAB" + str(num_ac).zfill(11)

                        exists_asid = AccountStableIDs.objects.filter(
                            ega_box_text=ebt).count()

                        if exists_asid == 1:
                            AccountStableIDs.objects.all().filter(ega_box_text=ebt).delete()

                        create_accession = AccountStableIDs.objects.create(
                            ega_box_text=ebt, submission_boxes=sb, account_accession=ac)
                    #
                    post = form.save()
                    post.save()
                    return redirect('accounts:create_account', pk=post.pk)

                else:
                    return redirect('accounts:account_detail', pk=post.pk)
            else:
                delete_check = SubmissionAccounts.objects.all().filter(
                    ega_box_number=ega_box_number).delete()
                delete_ac = AccountStableIDs.objects.all().filter(
                    ega_box_text="ega-box-"+str(ega_box_number)).delete()
                post = form.save()
                post.save()
                return redirect('accounts:submissionboxes_list')

    else:
        form = ModifySubBoxesModelForm(instance=post)
    return render(request, template_name, {'form': form, 'date': date})
