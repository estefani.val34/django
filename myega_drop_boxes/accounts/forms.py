from django import forms
from django.forms import (formset_factory, modelformset_factory)
from .models import (   
    SubmissionBoxes, 
    User, 
    SubmissionAccounts,
    Country,
    EgaBoxNumbersAvailable)
    
from django.forms import ComboField


FAVORITE_SUBMISSIONTYPE_CHOICES = [
    ('sequence', 'Sequence'),
    ('array', 'Array'),
    ('phenotype', 'Phenotype'),
]


class EgaBoxNumbersAvailableModelForm(forms.ModelForm):
    class Meta:
        model = EgaBoxNumbersAvailable
        fields = ('__all__')


class SubmissionAccountsModelForm(forms.ModelForm):
    submission_type = forms.MultipleChoiceField(
        required=True,
        widget=forms.CheckboxSelectMultiple,
        choices=FAVORITE_SUBMISSIONTYPE_CHOICES,
    )

    class Meta:
        model = SubmissionAccounts
        fields = ('__all__')
        exclude = ['country','mapi', 'affiliation_project', 'created_date', 'submission_box']

CountryFormset = modelformset_factory(
    Country,
    fields=('name', ),
    extra=1,
    widgets={'name': forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Enter Country Name here'
        })
    }
)


class FindSubmissionAccountForm(forms.Form):
    ega_box = forms.IntegerField(required=True)


class ModifySubBoxesModelForm(forms.ModelForm):
  
    class Meta:
        model = SubmissionBoxes
        fields = ('ega_box_number', 'rt_ticket', 'emails',
                   'received_dta', 'comments')

    


class SubmissionBoxesModelForm(forms.ModelForm):

    """ega_box_number = forms.NumberInput()

    def __init__(self, *args, **kwargs):
        super(SubmissionBoxesModelForm, self).__init__(*args, **kwargs)
        self.initial['ega_box_number'] = 34567
    """

    class Meta:
        model = SubmissionBoxes
        fields = ('ega_box_number', 'rt_ticket', 'comments')
        labels = {
            'ega_box_number': 'Ega Box Number',
            'rt_ticket': 'Rt Ticket',
            'comments': 'Comments'
        }
        widgets = {
            'ega_box_number': forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': '1234'
            }
            ),
            'rt_ticket': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter rt ticket here'
            }
            ),
            'comments': forms.Textarea(attrs={
                'class': 'form-control'
            }
            )
        }


UserFormset = modelformset_factory(
    User,
    fields=('email', ),
    extra=0,
    min_num=1, validate_min=True,
    widgets={
        'email': forms.EmailInput(attrs={
            'class': 'form-control',
            'placeholder': 'Enter email here'
        }
        )
    }

)
