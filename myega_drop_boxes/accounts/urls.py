from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'accounts'

urlpatterns = [
    path('', views.create_submissionboxes_with_users, name='create_submissionboxes_with_users'),
    path('submissionboxes/list', views.SubmissionboxesListView.as_view(), name='submissionboxes_list'),
    path('accounts/list', views.SubmissionAccountListView.as_view(), name='account_list'),
    path('stablesids/list', views.AccountStableIDsListView.as_view(), name='stablesidst_list'),
    path('numbersavailable/list', views.EgaBoxNumbersAvailableListView.as_view(), name='numbersavailable_list'),
    path('detail/<pk>/', views.SubmissionBoxesDetailView.as_view(), name='submissionboxes_detail'),
    path('account/<pk>/', views.SubmissionAccountDetailView.as_view(), name='account_detail'),
    path('detail/account/<pk>/', views.modify_submissionboxes, name='sub_detail'),
    path('create/account/<pk>/', views.create_account, name='create_account'),
    path('find', views.find_account , name='find_account'),
    url(r'^search$', views.search , name='search'),
    #url(r'^searchega$', views.searchega , name='searchega'),
]
